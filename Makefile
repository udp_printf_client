
CC=gcc
CFLAGS=-Wall -O2 -g
LDFLAGS=
SRC_udp_printf_client=udp_printf_client.c
OBJ_udp_printf_client=$(SRC_udp_printf_client:.c=.o)
TARGET_udp_printf_client=udp_printf_client

all: $(TARGET_udp_printf_client)

$(TARGET_udp_printf_client): $(OBJ_udp_printf_client)
	$(CC) $(LDFALGS) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(TARGET_udp_printf_client) $(OBJ_udp_printf_client)
