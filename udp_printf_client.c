
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define	UDP_PRINTF_DEFAULT_PORT		9999

/*
 * usage
 */
static void usage(const char *progname)
{
	fprintf(stderr, "usage: %s [port]\n", progname);

	exit(1);
}

/*
 * main
 */
int main(int argc, char **argv)
{
	char *endptr;
	int port, sock, error, n;
	struct sockaddr_in sockaddr;
	char buf[4096];

	port = UDP_PRINTF_DEFAULT_PORT;

	if (argc > 1) {
		port = strtol(argv[1], &endptr, 0);
		if ((*endptr != '\0') || (port < 0) || (port > 65535))
			usage(argv[0]);
	}

	sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		perror("bind");
		return 1;
	}

	memset(&sockaddr, 0, sizeof(sockaddr));
	sockaddr.sin_family = AF_INET;
	sockaddr.sin_addr.s_addr = INADDR_ANY;
	sockaddr.sin_port = htons(port);

	error = bind(sock, (const struct sockaddr*) &sockaddr, sizeof(sockaddr));
	if (error) {
		perror("bind");
		return 1;
	}

	while (1) {
		n = read(sock, buf, sizeof(buf) - 1);
		if (n <= 0)
			continue;

		buf[n] = 0;

		fprintf(stdout, buf, strlen(buf));
		fflush(stdout);
	}

	return 0;
}
